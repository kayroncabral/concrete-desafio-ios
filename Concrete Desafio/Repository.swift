//
//  Repository.swift
//  Concrete Desafio
//
//  Created by Kayron Cabral on 12/09/17.
//  Copyright © 2017 Kayron Cabral. All rights reserved.
//

import ObjectMapper

class Repository: Mappable {
    
    var id: Int!
    var name: String!
    var fullName: String!
    var longDescription: String?
    var forksCount: Int = 0
    var stargazersCount: Int = 0
    var owner: User!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        fullName <- map["full_name"]
        longDescription <- map["description"]
        forksCount <- map["forks_count"]
        stargazersCount <- map["stargazers_count"]
        owner <- map["owner"]
    }
    
}
