//
//  PullRequestCell.swift
//  Concrete Desafio
//
//  Created by Kayron Cabral on 13/09/17.
//  Copyright © 2017 Kayron Cabral. All rights reserved.
//

import UIKit

class PullRequestCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var body: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var login: UILabel!
    @IBOutlet weak var userName: UILabel!
    
    static let identifier = "PullRequest"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userName.text = ""
    }

    override func prepareForReuse() {
        avatar.image = nil
        userName.text = ""
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
