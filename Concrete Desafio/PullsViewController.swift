//
//  PullsViewController.swift
//  Concrete Desafio
//
//  Created by Kayron Cabral on 13/09/17.
//  Copyright © 2017 Kayron Cabral. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import AlamofireObjectMapper

class PullsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITableViewDataSourcePrefetching {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    static let identifier = "PullsViewController"
    
    /*
     The ImageDownloader uses a combination of an URLCache and AutoPurgingImageCache
     to create a very robust, high performance image caching system.
     */
    let downloader = ImageDownloader()
    
    var repositoryFullName: String!
    var pulls = [PullRequest]() {
        didSet {
            let hasItems = pulls.count > 0
            tableView.isHidden = !hasItems
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        loadPulls()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: animated)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pulls.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PullRequestCell.identifier, for: indexPath) as! PullRequestCell
        
        let pull = pulls[indexPath.row]
        
        cell.title.text = pull.title
        cell.date.text = pull.createdAt.toShortDate()
        cell.body.text = pull.body
        
        downloadAvatar(indexPath)
        
        cell.login.text = pull.user.login
        
        if let name = pull.user.name {
            cell.userName.text = name
        } else {
            downloadInfoUser(indexPath)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pull = pulls[indexPath.row]
        if let url = URL(string: pull.htmlUrl) {
            UIApplication.shared.openURL(url)
        }
    }

    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        indexPaths.forEach { (indexPath) in
            downloadAvatar(indexPath)
            downloadInfoUser(indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, cancelPrefetchingForRowsAt indexPaths: [IndexPath]) {
        indexPaths.forEach { (indexPath) in
            cancelDownloadAvatar(indexPath)
            cancelDownloadInfoUser(indexPath)
        }
    }

    //MARK: - Request
    
    func loadPulls() {
        activityIndicator.startAnimating()
        
        Alamofire.request(Route.pulls.replacingOccurrences(of: "{fullName}", with: repositoryFullName) + Param.clientAndSecret).responseArray { (response: DataResponse<[PullRequest]>) in
            if let pulls = response.result.value {
                self.pulls = pulls
                self.tableView.reloadData()
            }
            self.activityIndicator.stopAnimating()
        }
    }
    
    //MARK: - Name User Download
    
    func downloadInfoUser(_ indexPath: IndexPath) {
        let pull = pulls[indexPath.row]
        
        Alamofire.request(pull.user.url + Param.clientAndSecret).responseObject { (response: DataResponse<User>) in
            if let user = response.result.value {
                pull.user.name = user.name
                if let cell = self.tableView.cellForRow(at: indexPath) as? PullRequestCell {
                    if let name = user.name {
                        cell.userName.text = name
                    }
                }
            }
        }
    }
    
    func cancelDownloadInfoUser(_ indexPath: IndexPath) {
        let pull = pulls[indexPath.row]
        
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach({ (task) in
                if task.originalRequest?.url?.absoluteString == pull.user.url {
                    task.cancel()
                }
            })
        }
    }
    
    //MARK: - Image Download
    
    func downloadAvatar(_ indexPath: IndexPath) {
        let pull = pulls[indexPath.row]
        let urlRequest = URLRequest(url: URL(string: pull.user.avatarUrl)!)
        
        downloader.download(urlRequest) { (response) in
            if let image = response.result.value {
                if let cell = self.tableView.cellForRow(at: indexPath) as? PullRequestCell {
                    cell.avatar.image = image
                }
            }
        }
    }
    
    func cancelDownloadAvatar(_ indexPath: IndexPath) {
        let url = pulls[indexPath.row].user.avatarUrl
        
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach({ (task) in
                if task.originalRequest?.url?.absoluteString == url {
                    task.cancel()
                }
            })
        }
    }
    
}
