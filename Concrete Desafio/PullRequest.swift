//
//  PullRequest.swift
//  Concrete Desafio
//
//  Created by Kayron Cabral on 13/09/17.
//  Copyright © 2017 Kayron Cabral. All rights reserved.
//

import ObjectMapper

class PullRequest: Mappable {
    
    var title: String!
    var body: String!
    var user: User!
    var createdAt: String!
    var htmlUrl: String!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        title <- map["title"]
        body <- map["body"]
        user <- map["user"]
        createdAt <- map["created_at"]
        htmlUrl <- map["html_url"]
    }
    
}
