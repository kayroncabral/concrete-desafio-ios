//
//  String.swift
//  Concrete Desafio
//
//  Created by Kayron Cabral on 13/09/17.
//  Copyright © 2017 Kayron Cabral. All rights reserved.
//

import Foundation

extension String {
    func toShortDate() -> String {
        let formater = DateFormatter()
        formater.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = formater.date(from: self)
        formater.dateFormat = "dd/MM/yyy"
        return formater.string(from: date!)
    }
}
