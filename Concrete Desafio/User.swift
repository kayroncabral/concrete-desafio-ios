//
//  User.swift
//  Concrete Desafio
//
//  Created by Kayron Cabral on 12/09/17.
//  Copyright © 2017 Kayron Cabral. All rights reserved.
//

import ObjectMapper

class User: Mappable {
    
    var url: String!
    var login: String!
    var name: String?
    var avatarUrl: String!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        url <- map["url"]
        login <- map["login"]
        name <- map["name"]
        avatarUrl <- map["avatar_url"]
    }
    
}
