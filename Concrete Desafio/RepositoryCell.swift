//
//  RepositoryCell.swift
//  Concrete Desafio
//
//  Created by Kayron Cabral on 12/09/17.
//  Copyright © 2017 Kayron Cabral. All rights reserved.
//

import UIKit

class RepositoryCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var longDescription: UILabel!
    @IBOutlet weak var forksCount: UILabel!
    @IBOutlet weak var stargazersCount: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var login: UILabel!
    @IBOutlet weak var ownerName: UILabel!
    
    static let identifier = "Repository"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ownerName.text = " "
    }

    override func prepareForReuse() {
        avatar.image = nil
        ownerName.text = " "
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
