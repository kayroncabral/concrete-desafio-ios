//
//  RepositoriesViewController.swift
//  Concrete Desafio
//
//  Created by Kayron Cabral on 12/09/17.
//  Copyright © 2017 Kayron Cabral. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import AlamofireObjectMapper

class RepositoriesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITableViewDataSourcePrefetching {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let downloader = ImageDownloader()
    
    var isLoading = false
    var pageNumber = 1
    var repositories = [Repository]() {
        didSet {
            let hasItems = repositories.count > 0
            tableView.isHidden = !hasItems
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        loadRepositories()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: animated)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RepositoryCell.identifier, for: indexPath) as! RepositoryCell
        
        let repository = repositories[indexPath.row]
        
        cell.name.text = repository.name
        cell.longDescription.text = repository.longDescription
        cell.forksCount.text = "\(repository.forksCount)"
        cell.stargazersCount.text = "\(repository.stargazersCount)"
        
        downloadAvatar(indexPath)
        
        cell.login.text = repository.owner.login
        
        if let name = repository.owner.name {
            cell.ownerName.text = name
        } else {
            downloadInfoUser(indexPath)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        indexPaths.forEach { (indexPath) in
            downloadAvatar(indexPath)
            downloadInfoUser(indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, cancelPrefetchingForRowsAt indexPaths: [IndexPath]) {
        indexPaths.forEach { (indexPath) in
            cancelDownloadAvatar(indexPath)
            cancelDownloadInfoUser(indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == repositories.count - 5 {
            if !isLoading {
                loadRepositories()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: PullsViewController.identifier, sender: self)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == PullsViewController.identifier {
            if let pullsViewController = segue.destination as? PullsViewController {
                pullsViewController.repositoryFullName = repositories[tableView.indexPathForSelectedRow!.row].fullName
            }
        }
    }
    
    //MARK: - Request
    
    func loadRepositories() {
        activityIndicator.startAnimating()
        isLoading = true
        Alamofire.request(Route.searchRepositories.replacingOccurrences(of: "{page}", with: "\(pageNumber)")).responseObject { (response: DataResponse<SearchResponse>) in
            if let searchResponse = response.result.value {
                if let respositories = searchResponse.repositories {
                    self.repositories += respositories
                    self.pageNumber += 1
                    self.tableView.reloadData()
                }
            }
            self.isLoading = false
            self.activityIndicator.stopAnimating()
        }
    }
    
    //MARK: - Info User Download
    
    func downloadInfoUser(_ indexPath: IndexPath) {
        let repository = repositories[indexPath.row]
        
        Alamofire.request(repository.owner.url + Param.clientAndSecret).responseObject { (response: DataResponse<User>) in
            if let owner = response.result.value {
                repository.owner.name = owner.name
                if let cell = self.tableView.cellForRow(at: indexPath) as? RepositoryCell {
                    if let name = owner.name {
                        cell.ownerName.text = name
                    }
                }
            }
        }
    }
    
    func cancelDownloadInfoUser(_ indexPath: IndexPath) {
        let repository = repositories[indexPath.row]
        
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach({ (task) in
                if task.originalRequest?.url?.absoluteString == repository.owner.url {
                    task.cancel()
                }
            })
        }
    }
    
    //MARK: - Image Download
    
    /*
     The ImageDownloader uses a combination of an URLCache and AutoPurgingImageCache
     to create a very robust, high performance image caching system.
    */
    
    func downloadAvatar(_ indexPath: IndexPath) {
        let repository = repositories[indexPath.row]
        let urlRequest = URLRequest(url: URL(string: repository.owner.avatarUrl)!)
        
        downloader.download(urlRequest) { (response) in
            if let image = response.result.value {
                if let cell = self.tableView.cellForRow(at: indexPath) as? RepositoryCell {
                    cell.avatar.image = image
                }
            }
        }
    }
    
    func cancelDownloadAvatar(_ indexPath: IndexPath) {
        let url = repositories[indexPath.row].owner.avatarUrl
        
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.forEach({ (task) in
                if task.originalRequest?.url?.absoluteString == url {
                    task.cancel()
                }
            })
        }
    }
    
}
