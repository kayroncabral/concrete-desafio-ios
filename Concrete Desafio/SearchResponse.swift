//
//  SearchResponse.swift
//  Concrete Desafio
//
//  Created by Kayron Cabral on 12/09/17.
//  Copyright © 2017 Kayron Cabral. All rights reserved.
//

import ObjectMapper

class SearchResponse: Mappable {
    
    var repositories: [Repository]?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        repositories <- map["items"]
    }
    
}
