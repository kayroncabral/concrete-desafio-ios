//
//  Route.swift
//  Concrete Desafio
//
//  Created by Kayron Cabral on 13/09/17.
//  Copyright © 2017 Kayron Cabral. All rights reserved.
//

import Foundation

class Route {
    
    private static let baseUrl = "https://api.github.com"
    
    static var searchRepositories = baseUrl + "/search/repositories?q=language:Java&sort=stars&page={page}"
    static var pulls = baseUrl + "/repos/{fullName}/pulls"
    
}

class Param {
    
    static var clientAndSecret = "?client_id=5af9b43fd179b5b92542&client_secret=3e9952bd25cf67f9fdb1ce0245b58a52ec418364"
    
}
