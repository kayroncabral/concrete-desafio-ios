//
//  Concrete_DesafioUITests.swift
//  Concrete DesafioUITests
//
//  Created by Kayron Cabral on 12/09/17.
//  Copyright © 2017 Kayron Cabral. All rights reserved.
//

import XCTest

class Concrete_DesafioUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testRotateDevice() {
        let device = XCUIDevice.shared()
        sleep(5)
        device.orientation = .landscapeLeft
        sleep(3)
        device.orientation = .portrait
        let app = XCUIApplication()
        app.otherElements["repositoriesView"].tables.cells.element(boundBy: 0).tap()
        sleep(3)
        device.orientation = .landscapeLeft
        sleep(3)
        device.orientation = .portrait
    }
    
    func testTapCells() {
        let app = XCUIApplication()
        sleep(5)
        app.otherElements["repositoriesView"].tables.cells.element(boundBy: 0).tap()
        sleep(5)
        app.otherElements["pullsView"].tables.cells.element(boundBy: 0).tap()
    }
    
}
